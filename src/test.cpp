#include <test.hpp>

void test::deposit(name from, name to, eosio::asset quantity, std::string memo)
{

  //check(get_self() != from, "Sending funds from same account");
  
  exchanges_table _exchanges(get_self(), get_self().value);

  _exchanges.emplace(get_self(), [&](auto& exc) {
    exc.id = _exchanges.available_primary_key();
    exc.user = from;
    exc.from = quantity;
    exc.to = quantity;
  });

   action{
      permission_level{get_self(), "active"_n},
      "eosio.token"_n,
      "transfer"_n,
      std::make_tuple(get_self(), to, quantity, std::string("Test"))
  }.send();

}