#pragma once

#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>
#include <eosio/action.hpp>
#include <eosio/transaction.hpp>

using namespace std;
using namespace eosio;

CONTRACT test : public contract {
  public:
    using contract::contract;
    
    [[eosio::on_notify("eosio.token::transfer")]]
    void deposit(name from, name to, eosio::asset quantity, std::string memo);

    using transfer_action = action_wrapper<name("transfer"), &test::deposit>;

  private:
    TABLE exchanges {
      uint64_t id;
      name    user;
      asset from;
      asset to;
      auto primary_key() const { return id; }
    };
    typedef multi_index<name("exchanges"), exchanges> exchanges_table;
};
